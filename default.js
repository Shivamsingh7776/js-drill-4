const testObject = { name: undefined, age: 36, location: 'Gotham' };
function defaults(obj, defaultProps) {
    let ans = {};

    for(let index in obj){
        if(obj[index] == undefined){
            obj[index] = defaultProps[index];
        }

    }
    return obj;
}
const testObject1 = { name: 'shivam', age: 36, location: 'Gotham' };
let answer = defaults(testObject, testObject1);
console.log(answer);

module.exports = defaults;