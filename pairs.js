const testObject = { name: 'Bruce Wayne', age: 36, location: 'Gotham' };


function pairs(obj) {
    let ans = [];

    for(let index in obj){
        ans.push([index,obj[index]]);
    }
    return ans;
}

const answer = pairs(testObject);

console.log(answer);

module.exports = pairs;