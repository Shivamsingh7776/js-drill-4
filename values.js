const testObject = { name: 'Bruce Wayne', age: 36, location: 'Gotham' };

function values(obj) {
    let ans = [];

    for(let value in obj){
        ans.push(obj[value]);
    }
    return ans;
}

let answer = values(testObject);
console.log(answer);

module.exports = values;