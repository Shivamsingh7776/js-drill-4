const testObject = { name: undefined, age: 36, location: 'Gotham' };
const testObject1 = { name: 'shivam', age: 36, location: 'Gotham' };

const data = require('./default');

const expected_answer = { name: 'shivam', age: 36, location: 'Gotham' };

test("Deafault testing", () => {
    expect(data(testObject, testObject1)).toStrictEqual(expected_answer);
});