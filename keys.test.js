
const data = require('./keys');
const testObject = { name: 'Bruce Wayne', age: 36, location: 'Gotham' };
const expected_answer = [ 'name', 'age', 'location' ];

test("Keys testing", () =>{
    expect(data(testObject)).toStrictEqual(expected_answer);
});