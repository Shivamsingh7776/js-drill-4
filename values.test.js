
const data = require('./values');
const testObject = { name: 'Bruce Wayne', age: 36, location: 'Gotham' };
const expected_answer = [ 'Bruce Wayne', 36, 'Gotham' ];
test("Value testing", () =>{
    expect(data(testObject)).toStrictEqual(expected_answer);
});