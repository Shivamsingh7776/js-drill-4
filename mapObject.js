const testObject = { name: 'Bruce Wayne', age: 36, location: 'Gotham' };

function mapObject(obj, cb) {
    let ans = [];

    for(let index in obj){
        ans.push((cb(obj[index])));
    }
    return ans;
}



let answer = mapObject(testObject, (object) => {
    return object + 12;
});

console.log(answer);

module.exports = mapObject;