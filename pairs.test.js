const testObject = { name: 'Bruce Wayne', age: 36, location: 'Gotham' };

const expected_answer = [ [ 'name', 'Bruce Wayne' ], [ 'age', 36 ], [ 'location', 'Gotham' ] ]

const data = require('./pairs');

test("Pair testing", () =>{
    expect(data(testObject)).toStrictEqual(expected_answer);
});