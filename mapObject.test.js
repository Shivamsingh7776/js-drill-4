const testObject = { name: 'Bruce Wayne', age: 36, location: 'Gotham' };

const data = require('./mapObject');

const expected_answer = [ 'Bruce Wayne12', 48, 'Gotham12' ];

test("Mapobject testing", () => {
    expect(data(testObject, (object) => {
        return object + 12;
    })).toStrictEqual(expected_answer);
});