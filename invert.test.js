const testObject = { name: 'Bruce Wayne', age: 36, location: 'Gotham' };


const data = require('./invert');

const expected_answer = { '36': 'age', 'Bruce Wayne': 'name', Gotham: 'location' }

test("Invert testing", () => {
    expect(data(testObject)).toStrictEqual(expected_answer);
})