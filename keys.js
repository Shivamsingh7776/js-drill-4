const testObject = { name: 'Bruce Wayne', age: 36, location: 'Gotham' };
function keys(obj) {
    let ans = [];
    
    for(let key in obj){
        ans.push(key);
    }
    
    return ans;
}
let answer = keys(testObject);

console.log(answer);

module.exports = keys;